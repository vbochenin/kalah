package kalah.rest.converter;

import kalah.domain.Board;
import kalah.rest.dto.BoardDto;
import kalah.rest.dto.CellDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BoardToDto {

    public BoardDto convert(Board board) {
        BoardDto result = new BoardDto();
        List<CellDto> cells = new ArrayList<>(board.size());
        for (int i = 0; i < board.size(); i++) {
            CellDto cell = new CellDto();
            cell.setIndex(i);
            cell.setScore(board.getScoreAt(i));
            cells.add(cell);
        }
        result.setCells(cells);
        return result;
    }
}

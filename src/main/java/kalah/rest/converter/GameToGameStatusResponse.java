package kalah.rest.converter;

import kalah.domain.Game;
import kalah.rest.response.GameStatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class GameToGameStatusResponse {

    @Autowired
    private PlayerToDto playerToDto;

    @Autowired
    private BoardToDto boardToDto;

    public GameStatusResponse convert(Game game) {
        GameStatusResponse response = new GameStatusResponse();
        response.setActivePlayer(playerToDto.convert(game.getActivePlayer()));
        response.setFinished(game.isFinished());
        if (game.isFinished()) {
            response.setWinner(playerToDto.convert(game.getWinner()));
        }
        response.setBoard(boardToDto.convert(game.getBoard()));
        response.setPlayers(
            game.getPlayers()
                .stream()
                .map(playerToDto::convert)
                .collect(Collectors.toList())
        );
        return response;
    }
}

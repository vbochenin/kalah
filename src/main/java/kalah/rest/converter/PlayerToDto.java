package kalah.rest.converter;

import kalah.domain.Player;
import kalah.rest.dto.PlayerDto;
import org.springframework.stereotype.Component;

@Component
public class PlayerToDto {

    public PlayerDto convert(Player player) {
        PlayerDto result = new PlayerDto();
        result.setName(player.getName());
        return result;
    }
}

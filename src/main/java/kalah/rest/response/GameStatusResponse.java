package kalah.rest.response;

import kalah.domain.Player;
import kalah.rest.dto.BoardDto;
import kalah.rest.dto.PlayerDto;

import java.util.List;

public class GameStatusResponse {
    private PlayerDto activePlayer;

    private List<PlayerDto> players;

    private boolean finished;

    private PlayerDto winner;

    private BoardDto board;

    public PlayerDto getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(PlayerDto activePlayer) {
        this.activePlayer = activePlayer;
    }

    public List<PlayerDto> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerDto> players) {
        this.players = players;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public PlayerDto getWinner() {
        return winner;
    }

    public void setWinner(PlayerDto winner) {
        this.winner = winner;
    }

    public BoardDto getBoard() {
        return board;
    }

    public void setBoard(BoardDto board) {
        this.board = board;
    }
}

package kalah.rest;

import kalah.domain.Game;
import kalah.domain.Player;
import kalah.registerer.GameRegistrar;
import kalah.repository.GameRepository;
import kalah.rest.converter.GameToGameStatusResponse;
import kalah.rest.response.GameStatusResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.jayway.awaitility.Awaitility.await;
import static java.util.Objects.requireNonNull;

@RestController
public class KalahController {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private GameRegistrar gameRegistrar;

    @Autowired
    private GameToGameStatusResponse toGameStatusResponse;

    @RequestMapping(method = RequestMethod.GET, value = "/kalah/{gameName}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public GameStatusResponse getGameStatus(@PathVariable("gameName") String gameName) {
        return toGameStatusResponse.convert(
            requireNonNull(gameRepository.get(gameName), "Failed to get game with name: " + gameName)
        );
    }

    @RequestMapping(method = RequestMethod.POST, value = "/kalah/{gameName}/players/{playerName}",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public GameStatusResponse register(@PathVariable("gameName") String gameName,
        @PathVariable("playerName") String playerName)
    {
        Player player = new Player(playerName);
        gameRegistrar.register(gameName, player);
        await().forever().until(() -> gameRegistrar.allPlayersAreJoined(gameName));
        Player[] registeredPlayers = gameRegistrar.getPlayers(gameName).toArray(new Player[2]);

        Game game = gameRepository.create(new Game(gameName, registeredPlayers[0], registeredPlayers[1]));
        return toGameStatusResponse.convert(game);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/kalah/{gameName}/players/{playerName}/move/{cell}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public GameStatusResponse move(@PathVariable("gameName") String gameName,
        @PathVariable("playerName") String playerName,
        @PathVariable("cell") Integer cell)
    {
        Game game = requireNonNull(gameRepository.get(gameName), "Failed to get game with name: " + gameName);
        game.move(new Player(playerName), cell);
        return toGameStatusResponse.convert(game);
    }
}

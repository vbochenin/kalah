package kalah.rest.dto;

import java.util.List;

public class BoardDto {

    private List<CellDto> cells;

    public List<CellDto> getCells() {
        return cells;
    }

    public void setCells(List<CellDto> cells) {
        this.cells = cells;
    }
}

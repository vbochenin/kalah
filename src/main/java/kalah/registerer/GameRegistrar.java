package kalah.registerer;

import kalah.domain.Player;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class GameRegistrar {

    private final Map<String, Set<Player>> players = new ConcurrentHashMap<>();

    public void register(String gameName, Player player) {
        Set<Player> playersList = new HashSet<>();
        Set<Player> existed = players.putIfAbsent(gameName, playersList);
        if (existed != null) {
            playersList = existed;
        }
        if (allPlayersAreJoined(gameName)) {
            throw new IllegalArgumentException(String.format(
                    "All players are joined to game: %s", gameName
            ));
        }

        if (!playersList.add(player)) {
            throw new IllegalArgumentException(String.format(
                    "Player: %s is already registered for game: %s", player.getName(), gameName
            ));
        }
    }

    public boolean allPlayersAreJoined(String gameName) {
        return players.getOrDefault(gameName, Collections.emptySet()).size() == 2;
    }

    public Set<Player> getPlayers(String gameName) {
        return players.get(gameName);
    }
}

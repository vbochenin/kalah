package kalah.repository.impl;

import kalah.domain.Game;
import kalah.repository.GameRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InMemoryGameRepository implements GameRepository {

    private final Map<String, Game> gameCache = new ConcurrentHashMap<>();

    @Override
    public Game get(String name) {
        return gameCache.get(name);
    }

    @Override
    public Game create(Game newGame) {
        Game existed = gameCache.putIfAbsent(newGame.getName(), newGame);
        return existed == null ? newGame : existed;
    }
}

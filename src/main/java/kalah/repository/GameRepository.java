package kalah.repository;

import kalah.domain.Game;

import java.util.Set;

public interface GameRepository {

    Game get(String name);

    Game create(Game game);
}

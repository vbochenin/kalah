package kalah.domain;

public class Board {
    private static final int BOARD_SIZE = 14;
    private final int[] board;

    public Board() {
        this.board = new int[BOARD_SIZE];
    }

    public int getScoreAt(int cell) {
        validateCellRange(cell);
        return board[cell];
    }

    public int takeStonesFrom(int cell) {
        validateCellRange(cell);
        int stones = board[cell];
        board[cell] = 0;
        return stones;
    }

    public int putStonesInCell(int cell, int stones) {
        validateCellRange(cell);
        board[cell] += stones;
        return board[cell];
    }

    public int getNextCell(int cell) {
        validateCellRange(cell);
        cell++;
        if (cell >= BOARD_SIZE) {
            cell = 0;
        }
        return cell;
    }

    public int getOppositeCell(int cell) {
        return board.length - cell - 2;
    }

    public int size() {
        return board.length;
    }

    private void validateCellRange(int cell) {
        if (cell < 0 || cell > BOARD_SIZE - 1) {
            throw new IllegalArgumentException("Cell is not become to board");
        }
    }
}

package kalah.domain;

public class KalahPlayer extends Player {

    private int kalah;

    public KalahPlayer(String name, int kalah) {
        super(name);
        this.kalah = kalah;
    }

    public int getKalah() {
        return kalah;
    }

    public boolean isCellBelongs(int cell) {
        return cell < kalah && cell >= kalah - 6;
    }
}

package kalah.domain;

import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Objects;
import java.util.Random;

public class Game {
    private static final int FIRST_KALAH = 6;
    private static final int SECOND_KALAH = 13;
    private final String name;
    private final Board board;

    private final KalahPlayer firstPlayer;
    private final KalahPlayer secondPlayer;

    private KalahPlayer activePlayer;

    public Game(String name, Player firstPlayer, Player secondPlayer) {
        this.name = name;
        this.firstPlayer = new KalahPlayer(firstPlayer.getName(), FIRST_KALAH);
        this.secondPlayer = new KalahPlayer(secondPlayer.getName(), SECOND_KALAH);
        this.activePlayer = new Random().nextBoolean() ?
            this.firstPlayer :
            this.secondPlayer;
        this.board = createBoard();
    }

    public String getName() {
        return name;
    }

    public Board getBoard() {
        return board;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public List<Player> getPlayers() {
        return ImmutableList.of(firstPlayer, secondPlayer);
    }

    private Board createBoard() {
        Board board = new Board();
        for (int i = 0; i < board.size(); i++) {
            if (!isKalahCell(i)) {
                board.putStonesInCell(i, 6);
            }
        }
        return board;
    }

    public void move(Player player, int cell) {
        KalahPlayer kalahPlayer = toKalahPlayer(player);

        checkPlayerCanMakeTurn(kalahPlayer);
        checkPlayerCanMakeTurnFromCell(kalahPlayer, cell);

        int stones = board.takeStonesFrom(cell);
        while (stones > 1) {
            cell = board.getNextCell(cell);
            if (cell != getOpponent(kalahPlayer).getKalah()) {
                board.putStonesInCell(cell, 1);
                stones--;
            }
        }
        cell = putLastStoneInCell(kalahPlayer, cell);

        if (isFinished()) {
            moveRestStonesToKalah();
        }

        if (cell != kalahPlayer.getKalah()) {
            activePlayer = getOpponent(activePlayer);
        }
    }

    private int putLastStoneInCell(KalahPlayer player, int cell) {
        cell = board.getNextCell(cell);
        if (canCapture(player, cell)) {
            board.putStonesInCell(player.getKalah(), board.takeStonesFrom(board.getOppositeCell(cell)) + 1);
        } else {
            if (cell == getOpponent(player).getKalah()) {
                cell = board.getNextCell(cell);
            }
            board.putStonesInCell(cell, 1);
        }

        return cell;
    }

    private boolean canCapture(KalahPlayer player, int currentCell) {
        return player.isCellBelongs(currentCell) &&
            board.getScoreAt(currentCell) == 0 &&
            board.getScoreAt(board.getOppositeCell(currentCell)) > 0;
    }

    public boolean isFinished() {
        return capturedMoreThenHalfStones() || !ableToMakeTurn(firstPlayer) || !ableToMakeTurn(secondPlayer);
    }

    public Player getWinner() {
        if (!isFinished()) {
            return null;
        }

        int firstPlayerScores = board.getScoreAt(firstPlayer.getKalah());
        int secondPlayerScores = board.getScoreAt(secondPlayer.getKalah());
        if (firstPlayerScores > secondPlayerScores) {
            return firstPlayer;
        } else if (firstPlayerScores < secondPlayerScores) {
            return secondPlayer;
        } else {
            return null;
        }
    }

    private void moveRestStonesToKalah() {
        for (int i = 0; i < board.size(); i++) {
            if (!isKalahCell(i)) {
                board.putStonesInCell(getKalahForCell(i), board.takeStonesFrom(i));
            }
        }
    }

    private KalahPlayer toKalahPlayer(Player player) {
        if (Objects.equals(player.getName(), firstPlayer.getName())) {
            return firstPlayer;
        } else if (Objects.equals(player.getName(), secondPlayer.getName())) {
            return secondPlayer;
        }
        throw new IllegalArgumentException(
            String.format("Failed to find player: %s in game: %s", player.getName(), name)
        );
    }

    private void checkPlayerCanMakeTurn(KalahPlayer kalahPlayer) {
        if (!kalahPlayer.equals(activePlayer)) {
            throw new IllegalArgumentException("Player cannot make the turn. Player is not active");
        }
    }

    private boolean capturedMoreThenHalfStones() {
        return board.getScoreAt(firstPlayer.getKalah()) > 36 || board.getScoreAt(secondPlayer.getKalah()) > 36;
    }

    private boolean ableToMakeTurn(KalahPlayer player) {
        int start = player.getKalah() - 6;
        for (int i = start; i < start + 6; i++) {
            if (board.getScoreAt(i) != 0) {
                return true;
            }
        }
        return false;
    }

    private void checkPlayerCanMakeTurnFromCell(KalahPlayer kalahPlayer, int cell) {
        if (!kalahPlayer.isCellBelongs(cell)) {
            throw new IllegalArgumentException("Player cannot make the turn. Cell doesn't belongs to player");
        }
        if (isKalahCell(cell)) {
            throw new IllegalArgumentException("Player cannot make the turn. You cannot move from kalah cell");
        }
        if (board.getScoreAt(cell) == 0) {
            throw new IllegalArgumentException("Player cannot make the turn. You cannot move from empty cell");
        }
    }

    private int getKalahForCell(int cell) {
        return cell <= FIRST_KALAH ? FIRST_KALAH : SECOND_KALAH;
    }

    private boolean isKalahCell(int cell) {
        return cell == FIRST_KALAH || cell == SECOND_KALAH;
    }

    private KalahPlayer getOpponent(Player player) {
        if (Objects.equals(player.getName(), firstPlayer.getName())) {
            return secondPlayer;
        } else if (Objects.equals(player.getName(), secondPlayer.getName())) {
            return firstPlayer;
        }
        throw new IllegalStateException("Failed to find opponent");
    }
}

package kalah.registerer;

import kalah.domain.Player;
import org.junit.Test;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class GameRegistrarTest {

    private final GameRegistrar gameRegistrar = new GameRegistrar();


    @Test
    public void shouldRegisterPlayer() {
        gameRegistrar.register("game", new Player("player1"));

        assertThat(gameRegistrar.getPlayers("game"),
            containsInAnyOrder(new Player("player1")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotRegisterTheSamePlayerInTheSameGame() {
        gameRegistrar.register("game", new Player("player1"));
        gameRegistrar.register("game", new Player("player1"));
    }

    @Test
    public void shouldReturnAllPlayersJoined() {
        gameRegistrar.register("game", new Player("player1"));
        assertFalse(gameRegistrar.allPlayersAreJoined("game"));

        gameRegistrar.register("game", new Player("player2"));
        assertTrue(gameRegistrar.allPlayersAreJoined("game"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotRegisterPlayerWhenAllPlayersRegistered() {
        gameRegistrar.register("game", new Player("player1"));
        gameRegistrar.register("game", new Player("player2"));
        assertTrue(gameRegistrar.allPlayersAreJoined("game"));
        gameRegistrar.register("game", new Player("player3"));
    }
}
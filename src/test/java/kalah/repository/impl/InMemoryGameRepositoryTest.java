package kalah.repository.impl;

import kalah.domain.Game;
import kalah.domain.Player;
import org.junit.Test;

import static org.junit.Assert.assertSame;

public class InMemoryGameRepositoryTest {


    private final InMemoryGameRepository inMemoryGameRepository = new InMemoryGameRepository();

    @Test
    public void shouldCreateGame() {
        Game game = new Game("game", new Player("1"), new Player("2"));

        inMemoryGameRepository.create(game);

        assertSame(inMemoryGameRepository.get("game"), game);
    }

    @Test
    public void shouldNotCreateNewGameWhenItAlreadyExisted() {
        Game game = new Game("game", new Player("1"), new Player("2"));

        inMemoryGameRepository.create(game);

        Game game2 = new Game("game", new Player("2"), new Player("3"));

        assertSame(game, inMemoryGameRepository.create(game2));

    }
}
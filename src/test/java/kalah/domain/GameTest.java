package kalah.domain;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class GameTest {

    private final Player firstPlayer = new Player("first");
    private final Player secondPlayer = new Player("second");

    private final Game game = new Game("any", firstPlayer, secondPlayer);


    @Test
    public void shouldCreateBoardWithStones() {
        assertThat(game.getBoard(), withCells(new int[] {6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0}));
    }

    @Test(expected = IllegalArgumentException.class)
    public void justActiveUserCanMakeMove() {
        Assert.assertNotNull(game.getActivePlayer());

        Player activePlayer = game.getActivePlayer();

        game.move(activePlayer.getName().equals(firstPlayer.getName()) ? secondPlayer : firstPlayer,
            toActivePlayerCell(activePlayer, 1));
    }

    @Test
    public void shouldPutStonesOnTheRightSide() {
        makeFirstUserIsActive();
        game.move(game.getActivePlayer(), 1);
        assertThat(game.getBoard(), withCells(new int[] {6, 0, 7, 7, 7, 7, 1, 7, 6, 6, 6, 6, 6, 0}));
    }

    private int toActivePlayerCell(Player activePlayer, int cell) {
        return activePlayer.getName().equals(firstPlayer.getName()) ? cell : cell + 6;
    }

    private void makeFirstUserIsActive() {
        if (!game.getActivePlayer().getName().equals(firstPlayer.getName())) {
            int stones = game.getBoard().takeStonesFrom(7);
            game.getBoard().putStonesInCell(7, 1);
            game.move(game.getActivePlayer(), 7);

            game.getBoard().putStonesInCell(7, stones);
            game.getBoard().putStonesInCell(8, game.getBoard().takeStonesFrom(8) - 1);
        }
    }

    @Test
    public void shouldNotSwitchActiveUserWhenLastStoneInOwnKalah() {
        makeFirstUserIsActive();
        Player activePlayer = game.getActivePlayer();
        game.move(activePlayer, 0);
        assertThat(game.getBoard(), withCells(new int[] {0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0}));

        Assert.assertEquals(activePlayer, game.getActivePlayer());
    }

    @Test
    public void shouldSwitchActiveUserWhenLastStoneInOwnKalah() {
        makeFirstUserIsActive();
        Player activePlayer = game.getActivePlayer();
        game.move(activePlayer, 2);
        assertThat(game.getBoard(), withCells(new int[] {6, 6, 0, 7, 7, 7, 1, 7, 7, 6, 6, 6, 6, 0}));

        assertNotEquals(activePlayer, game.getActivePlayer());
    }

    @Test
    public void shouldNotPutStoneInOpponentKalah() {
        makeFirstUserIsActive();
        game.getBoard().takeStonesFrom(5);
        game.getBoard().putStonesInCell(5, 8);
        game.move(game.getActivePlayer(), 5);
        assertThat(game.getBoard(), withCells(new int[] {7, 6, 6, 6, 6, 0, 1, 7, 7, 7, 7, 7, 7, 0}));
    }

    @Test
    public void shouldCaptureStonesOnOppositeCell() {
        makeFirstUserIsActive();

        game.getBoard().takeStonesFrom(4);
        game.getBoard().takeStonesFrom(5);
        game.getBoard().putStonesInCell(4, 1);

        game.move(game.getActivePlayer(), 4);
        assertThat(game.getBoard(), withCells(new int[] {6, 6, 6, 6, 0, 0, 7, 0, 6, 6, 6, 6, 6, 0}));

    }

    @Test
    public void shouldReturnWinnerWhenGameIsFinished() {
        makeFirstUserIsActive();

        assertFalse(game.isFinished());
        assertNull(game.getWinner());

        game.getBoard().takeStonesFrom(0);
        game.getBoard().takeStonesFrom(1);
        game.getBoard().takeStonesFrom(2);
        game.getBoard().takeStonesFrom(3);
        game.getBoard().takeStonesFrom(4);
        game.getBoard().takeStonesFrom(5);
        game.getBoard().putStonesInCell(6, 5);
        game.getBoard().takeStonesFrom(13);
        game.getBoard().putStonesInCell(13, 4);

        assertTrue(game.isFinished());
        assertEquals(firstPlayer.getName(), game.getWinner().getName());
    }

    @Test
    public void shouldNotReturnWinnerWhenScoreTheSame() {
        makeFirstUserIsActive();

        assertFalse(game.isFinished());
        assertNull(game.getWinner());

        game.getBoard().takeStonesFrom(0);
        game.getBoard().takeStonesFrom(1);
        game.getBoard().takeStonesFrom(2);
        game.getBoard().takeStonesFrom(3);
        game.getBoard().takeStonesFrom(4);
        game.getBoard().takeStonesFrom(5);
        game.getBoard().putStonesInCell(6, 5);
        game.getBoard().takeStonesFrom(13);
        game.getBoard().putStonesInCell(13, 5);

        assertTrue(game.isFinished());
        assertNull(game.getWinner());
    }

    @Test
    public void shouldReturnGameIsFinishedWhenPlayerNotAbleToMakeTurn() {
        makeFirstUserIsActive();

        assertFalse(game.isFinished());

        game.getBoard().takeStonesFrom(0);
        game.getBoard().takeStonesFrom(1);
        game.getBoard().takeStonesFrom(2);
        game.getBoard().takeStonesFrom(3);
        game.getBoard().takeStonesFrom(4);
        game.getBoard().takeStonesFrom(5);

        assertTrue(game.isFinished());
    }

    @Test
    public void shouldReturnGameIsFinishedWhenPlayerCaptureMoreThanHalfStones() {
        makeFirstUserIsActive();

        assertFalse(game.isFinished());

        game.getBoard().putStonesInCell(6, 37);

        assertTrue(game.isFinished());
    }

    @Test
    public void shouldMoveREstStonesWhenGameIsFinished() {
        makeFirstUserIsActive();

        assertFalse(game.isFinished());

        game.getBoard().takeStonesFrom(0);
        game.getBoard().takeStonesFrom(1);
        game.getBoard().takeStonesFrom(2);
        game.getBoard().takeStonesFrom(3);
        game.getBoard().takeStonesFrom(4);
        game.getBoard().takeStonesFrom(5);
        game.getBoard().putStonesInCell(5, 1);

        game.move(game.getActivePlayer(), 5);

        assertThat(game.getBoard(), withCells(new int[] {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 36}));
    }


    private static Matcher<Board> withCells(int[] cells) {
        return new TypeSafeMatcher<Board>() {
            @Override
            protected boolean matchesSafely(Board board) {
                if (cells.length != board.size()) {
                    return false;
                }
                for (int i = 0; i < cells.length; i++) {
                    if (cells[i] != board.getScoreAt(i)) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }

}
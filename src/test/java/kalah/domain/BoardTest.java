package kalah.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BoardTest {

    private final Board board = new Board();


    @Test
    public void shouldCreateEmptyBoard() {
        assertEquals(14, board.size());
        for (int i = 0; i < board.size(); i++) {
            assertEquals(0, board.getScoreAt(i));
        }
    }

    @Test
    public void shouldPutStoneInCell() {
        board.putStonesInCell(4, 8);
        assertEquals(8, board.getScoreAt(4));

    }

    @Test
    public void shouldTakeStonesFromCell() {
        board.putStonesInCell(5, 42);
        board.putStonesInCell(6, 33);

        int stones = board.takeStonesFrom(5);
        assertEquals(42, stones);
        assertEquals(0, board.getScoreAt(5));
        assertEquals(33, board.getScoreAt(6));
    }

    @Test
    public void shouldReturnCellOnOppositeSide() {
        assertEquals(12, board.getOppositeCell(0));
        assertEquals(0, board.getOppositeCell(12));
        assertEquals(7, board.getOppositeCell(5));
    }

    @Test
    public void shouldReturnNextCellIndex() {
        assertEquals(1, board.getNextCell(0));
        assertEquals(6, board.getNextCell(5));
        assertEquals(0, board.getNextCell(13));
    }


}
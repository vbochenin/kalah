# README #

### What is this repository for? ###

* This repos is for coding task for BackBase;
The goal of this test is for us to have some code to chat about on the interview,
and for you to showcase your programming skills.

We are asking you to program a Java web application that runs a game of 6-stone Kalah. The general rules of the game are explained on Wikipedia: https://en.wikipedia.org/wiki/Kalah and also below in this document.
Please note that the Wikipedia article explains 3 and 4-stone Kalah; we would like your implementation to be 6-stone.

This web application should enable to let 2 human players play the game; there is no AI required. It doesn't need a fancy web interface or front end, it can also be a service endpoint.


### How do I get set up? ###

* Dependencies: Maven
* How to run: mvn spring-boot:run


### Rest API ###

All API endpoints return response in same format in case of success:

* activePlayer: {}  - player who able to make a turn
* players : [] - list of players
* finished : boolean - is game finished?
* winner: {} - return winner if game is finished. Null if players have same scores or game is not finished
* board: {cells: [{index: , score: }] - state of board after move

For example:

```
{"activePlayer":{"name":"2"},"players":[{"name":"1"},{"name":"2"}],"finished":false,"winner":null,"board":{"cells":[{"index":0,"score":6},{"index":1,"score":6},{"index":2,"score":6},{"index":3,"score":6},{"index":4,"score":6},{"index":5,"score":6},{"index":6,"score":0},{"index":7,"score":6},{"index":8,"score":6},{"index":9,"score":6},{"index":10,"score":6},{"index":11,"score":6},{"index":12,"score":6},{"index":13,"score":0}]}}
```

API:

* Get game status: GET /kalah/{gameName}
* Register player in game: POST /kalah/{gameName}/players/{playerName}
* Make move: POST /kalah/{gameName}/players/{playerName}/move/{cellIndex}
